from bs4 import BeautifulSoup
from pandas import DataFrame

from errors import WebpageLoadError
from utils import Utils


class WeekMeteoInfo:
    def __init__(self, page_source: str):
        self.page_source = page_source

    def get_week_meteoinfo(self) -> list[DataFrame, str]:
        """
        Собирает данные о погоде на неделю и время обновления страницы
        :return: list[DataFrame, str]
        """
        soup = BeautifulSoup(self.page_source, 'html.parser')

        tab_page = soup.find('div', attrs={'class': 'tab-page'})

        tr = tab_page.find_all('tr')

        # получаем дни недели
        days_tab = tr[0]

        today = [days_tab.find_all('td')[2].text]

        next_days = [day.text for day in days_tab.find_all('td', attrs={'class': 'pogodadate'})]

        days_of_week = today + next_days

        # форматируем дни недели для красоты
        days_of_week = Utils.format_days_of_week(days_of_week)

        # получаем атмосферное давление ночью\днем
        pressure_tab = tr[2]

        pressure_week_info = [el.text for el in pressure_tab.find_all('td', attrs={'class': 'pogodacell'})]

        pressure_week_info = Utils.format_pressure_week_info(pressure_week_info)

        # получаем температуру воздуха ночью / днем

        temperature_tab = tr[3]

        temperature_week_info = [el.text for el in temperature_tab.find_all('td', attrs={'class': 'pogodacell'})]

        temperature_week_info = Utils.format_temperature_week_info(temperature_week_info)

        # получаем комментарий в погоде

        comment_weather_tab = tr[5]

        comment_week_info = [el.text for el in comment_weather_tab.find_all('td', attrs={'class': 'pogodacell'})]

        # получаем осадки, мм

        precipitation_tab = tr[6]

        precipitation_week_info = [el.text for el in precipitation_tab.find_all('td', attrs={'class': 'pogodacell'})]

        # получаем вероятность осадков, %

        probability_precipitation_tab = tr[7]

        probability_precipitation_week_info = [el.text for el in
                                               probability_precipitation_tab.find_all('td',
                                                                                      attrs={'class': 'pogodacell'})]

        # получаем направление ветра

        wind_direction_tab = tr[8]

        wind_direction_week_info = [el.text for el in wind_direction_tab.find_all('td', attrs={'class': 'pogodacell'})]

        # получаем скорость ветра, м/с

        wind_speed_tab = tr[9]

        wind_speed_week_info = [el.text for el in wind_speed_tab.find_all('td', attrs={'class': 'pogodacell'})]

        data = {
            'days': days_of_week,
            'pressure': pressure_week_info,
            'temperature': temperature_week_info,
            'comment': comment_week_info,
            'precipitation': precipitation_week_info,
            'probability_precipitation': probability_precipitation_week_info,
            'wind_direction': wind_direction_week_info,
            'wind_speed': wind_speed_week_info
        }

        return [Utils.create_data_frame(data), self.get_last_update()]

    def get_last_update(self) -> str:
        """
        Получает время последнего обновления
        :return: str
        """
        soup = BeautifulSoup(self.page_source, 'html.parser')

        page_text = soup.text

        if 'Страница была обновлена' in page_text:
            index1 = page_text.index('Страница была обновлена')
            index2 = page_text.index('(моск.врем.)')

            return page_text[index1:index2] + '(моск.врем.)'

        raise WebpageLoadError('Ошибка загрузки страницы')

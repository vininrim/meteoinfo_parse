import requests
from bs4 import BeautifulSoup
from pandas import DataFrame
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service

from errors import WebpageLoadError
from utils import Utils


class SeleniumUse:
    def __init__(self, url: str):
        self.url = url

        self.service = Service(executable_path='webdriver/geckodriver.exe')

        self.options = Options()
        self.options.set_preference('dom.webdriver.enabled', False)
        self.options.set_preference('dom.webnotifications.enabled', False)
        self.options.set_preference('media.volume_scale', '0.0')
        self.options.headless = True

        self.browser = webdriver.Firefox(service=self.service, options=self.options)

        self.action = ActionChains(self.browser)

    def get_days_meteoinfo(self) -> list[DataFrame, str]:
        """
        Собирает почасовые данные о погоде на несколько дней
        :return: list[DataFrame, str]
        """
        self.browser.get(self.url)

        xpath_button = '//*[@id="tab-pane-1"]/div[1]/h2[2]/a'

        self.browser.find_element(By.XPATH, xpath_button).click()

        elements_tab = self.browser.find_element(By.TAG_NAME, 'svg')

        image_elements = elements_tab.find_elements(By.TAG_NAME, 'image')

        """
        class  highcharts-tooltip
        """

        days_and_times = []

        description = []

        temperature = []

        pressure = []

        humidity = []

        precipitation = []

        probability_precipitation = []

        wind_direction = []

        wind_speed = []

        for i in image_elements:
            self.action.move_to_element(i).perform()

            page_source = self.browser.page_source

            soup = BeautifulSoup(page_source, 'html.parser')

            days_and_times.append(soup.select('#highcharts-0 > div > span > small')[0].text)

            try:
                description.append(soup.select('#highcharts-0 > div > span > b')[0].text)
            except Exception:
                description.append(None)

            temperature.append(
                soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(1) > td:nth-child(2)')[0].text.split(
                    ' ')[0]
            )

            pressure.append(
                soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(2) > td:nth-child(2)')[0].text.split(
                    ' ')[0]
            )

            humidity.append(
                soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(3) > td:nth-child(2)')[0].text.split(
                    ' ')[0]
            )

            if 'Ветер' in ''.join([str(i) for i in soup.select(
                    '#highcharts-0 > div > span > table > tbody > tr:nth-child(4) > td:nth-child(1)')]):
                wind_direction.append(
                    Utils.f(soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(4) > td:nth-child(2)')[
                          0].text.split(' ')[0])[0]
                )
                # print(soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(4) > td:nth-child(2)')[0].text)
                wind_speed.append(
                    Utils.f(soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(4) > td:nth-child(2)')[
                          0].text.split(' ')[0])[1]
                )

                precipitation.append(None)
                probability_precipitation.append(None)

            elif 'Осадки' in ''.join([str(i) for i in soup.select(
                    '#highcharts-0 > div > span > table > tbody > tr:nth-child(4) > td:nth-child(1)')]):
                precipitation.append(
                    soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(4) > td:nth-child(2)')[
                        0].text.split(' ')[0]
                )

                try:
                    probability_precipitation.append(
                        soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(5) > td:nth-child(2)')[
                            0].text.split(' ')[0]
                    )
                except Exception:
                    probability_precipitation.append(None)

            if 'Ветер' in ''.join([str(i) for i in soup.select(
                    '#highcharts-0 > div > span > table > tbody > tr:nth-child(6) > td:nth-child(1)')]):
                wind_direction.append(
                    Utils.f(soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(6) > td:nth-child(2)')[
                          0].text.split(
                        ' ')[0])[0]
                )
                wind_speed.append(
                    Utils.f(soup.select('#highcharts-0 > div > span > table > tbody > tr:nth-child(6) > td:nth-child(2)')[
                          0].text.split(
                        ' ')[0])[1]
                )

            else:
                wind_direction.append(None)
                wind_speed.append(None)

        self.browser.close()

        data = {
            'date': days_and_times,
            'description': description,
            'temperature': temperature,
            'pressure': pressure,
            'humidity': humidity,
            'precipitation': precipitation,
            'probability_precipitation': probability_precipitation,
            'wind_direction': wind_direction,
            'wind_speed': wind_speed
        }

        return [Utils.create_data_frame(data), self.get_last_update()]

    def get_last_update(self) -> str:
        """
        Данные обновления страницы
        :return: str - данные обновления страницы
        """
        res = requests.get(self.url)

        soup = BeautifulSoup(res.content, 'html.parser')

        page_text = soup.text

        if 'Страница была обновлена' in page_text:
            index1 = page_text.index('Страница была обновлена')
            index2 = page_text.index('(моск.врем.)')

            return page_text[index1:index2] + '(моск.врем.)'

        raise WebpageLoadError('Ошибка загрузки страницы')




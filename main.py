from all_cities import AllCities
from selenium_use import SeleniumUse
from utils import Utils
from week_meteoinfo import WeekMeteoInfo

domain = 'http://old.meteoinfo.ru'

# все города
all_cities = AllCities(Utils.get_page_source(domain))

all_cities_dataframe = all_cities.get_all_cities()

# погода ярославля на неделю
week_info = WeekMeteoInfo(Utils.get_page_source(domain + '/forecasts5000/russia/yaroslavl-area/jaroslavl'))

week_info = week_info.get_week_meteoinfo()  # DataFrame и время обновления страницы

week_info_dataframe = week_info[0]  # DataFrame

last_update = week_info[1]  # Время обновления

# погода на несколько дней по часам (работает медленно)
selen_use = SeleniumUse(domain + '/forecasts5000/russia/yaroslavl-area/jaroslavl')

days_info = selen_use.get_days_meteoinfo()  # DataFrame и время обновления страницы

days_info_dataframe = days_info[0]  # DataFrame

last_update = days_info[1]  # Время обновления страницы



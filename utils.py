import pandas as pd
import requests


class Utils:
    @staticmethod
    def get_page_source(url: str) -> str:
        """
        Получает html разметку страницы
        :param url: ссылка на страницу
        :return: html разметка страницы
        """
        res = requests.get(url)
        return res.content
    
    @staticmethod
    # не придумал адекватное название)
    def f(st: str) -> list:
        """
        Функция используется для форматирования данные скорости и направления ветра, полученных при помощи selenium
        :param st: строка с данными для форматирования
        :return: список с фарматироваыми данными
        """
        direction = ''
        wind_speed = ''

        if '-' in st:
            index = st.index('-')

            direction += st[index - 1: index + 2]
            wind_speed += st[index + 2:]

        else:
            direction += st[0]
            wind_speed += st[1:]

        return [direction, wind_speed]

    @staticmethod
    def format_days_of_week(days: list) -> list:
        """
        Форматирование дней недели
        :param days: список данных для форматирвания
        :return: форматированные данные
        """
        days_of_week = ['Понедельник',
                        'Вторник',
                        'Среда',
                        'Четверг',
                        'Пятница',
                        'Суббота',
                        'Воскресенье']

        formatted_days = []

        for day in days:
            context = day.split(' ')
            if len(context) == 2:
                start_index = None
                for check_day in days_of_week:
                    if check_day in context[0]:
                        start_index = len(context[0]) - len(check_day)

                if start_index is not None:
                    context[0] = f'{context[0][:-start_index]} {context[0][-start_index:]}'

                formatted_days.append(f'{context[0]} {context[1]}')
            else:
                return days

        return formatted_days

    @staticmethod
    def format_pressure_week_info(info: list) -> list:
        """
        Форматирвание данных о давлении
        :param info: список данных для форматирования
        :return: форматированные данные
        """
        formatted_info = []
        for el in info:
            context = el.split(' ')

            context[1] = '/'

            formatted_info.append(' '.join(context))

        return formatted_info

    @staticmethod
    def format_temperature_week_info(info: list) -> list:
        """
        Форматирвание данных о температуре
        :param info: список данных для форматирования
        :return: форматированные данные
        """
        formatted_info = []
        for el in info:
            context = el.split(' ')

            context[1] = '/'

            formatted_info.append(' '.join(context))

        return formatted_info

    @staticmethod
    def create_data_frame(data: dict):
        return pd.DataFrame(data)

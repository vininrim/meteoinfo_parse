from bs4 import BeautifulSoup
from pandas import DataFrame

from utils import Utils


class AllCities:
    def __init__(self, page_source: str):
        """
        :param page_source: html разметка страницы
        """
        self.page_source = page_source

    def get_all_cities(self) -> DataFrame:
        """
        получает названия всех городов и ссылки к ним
        :return: DataFrame
        """
        soup = BeautifulSoup(self.page_source, 'html.parser')

        tab_page = soup.find('div', attrs={'class': 'tab-page'})

        td = tab_page.find_all('td')

        data_city_name = []
        data_city_url = []

        for element in td:
            a = element.find('a')

            data_city_name.append(a.text)
            data_city_url.append(a['href'])

        data = {
            'city_name': data_city_name,
            'city_url': data_city_url
        }

        return Utils.create_data_frame(data)
